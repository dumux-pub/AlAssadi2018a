// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Binarycoefficients
 * \brief Binary coefficients for nitrogen and oxygen.
 */
#ifndef DUMUX_BINARY_COEFF_CO2_H2_HH
#define DUMUX_BINARY_COEFF_CO2_H2_HH

#include <dumux/material/binarycoefficients/henryiapws.hh>
#include <dumux/material/binarycoefficients/fullermethod.hh>

#include <appl/material/components/h2.hh>
#include <appl/material/components/co2.hh>

namespace Dumux
{
namespace BinaryCoeff
{

/*!
 * \ingroup Binarycoefficients
 * \brief Binary coefficients for hydrogen and co2.
 */


template<class Scalar, class CO2Tables>
class CO2_H2
{
public:
    /*!
     * \brief Henry coefficient \f$\mathrm{[Pa]}\f$  for molecular hydrogen in liquid co2.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     */
    //template <class Scalar>
    static Scalar henry(Scalar temperature)
    {
        DUNE_THROW(Dune::NotImplemented, "henry coefficient for hydrogen in liquid co2");
    }

    /*!
     * \brief Binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular hydrogen in liquid co2.
     *
     * Uses fullerMethod to determine the diffusion of hydrogen in carbon dioxide.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
     * THE PROPERTIES OF GASES AND LIQUIDS 5th Edition Cap. 11.11
     */
    static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)
    {
        
        using CO2 = Dumux::Components::CO2<Scalar, CO2Tables>;
        using H2 = Dumux::Components::H2<Scalar>;

        // atomic diffusion volumes
        const Scalar SigmaNu[2] = { 26.9 /* CO2 */,  6.12 /* H2 */ };
        // molar masses [g/mol]
        const Scalar M[2] = { CO2::molarMass()*1e3, H2::molarMass()*1e3 };
        return fullerMethod(M, SigmaNu, temperature, pressure);
    }

    /*!
     * \brief Diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular hydrogen in liquid co2.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
        DUNE_THROW(Dune::NotImplemented, "diffusion coefficient for liquid carbon dioxide and hydrogen");
    }
};

}
} // end namespace

#endif
