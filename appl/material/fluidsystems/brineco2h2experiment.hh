// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 * \ingroup Fluidsystems
 * \brief @copybrief Dumux::FluidSystems::BRINECO2H2
 */
#ifndef DUMUX_Brine_CO2_H2_FLUID_SYSTEM_HH
#define DUMUX_Brine_CO2_H2_FLUID_SYSTEM_HH

#include <cassert>

#include <dumux/common/valgrind.hh>
#include <dumux/common/exceptions.hh>

#include <dumux/material/fluidsystems/base.hh>
#include <dumux/material/idealgas.hh>
#include <dumux/material/constants.hh>

#include <appl/material/components/co2.hh>
#include <appl/material/components/h2.hh>
#include <appl/material/components/brine.hh>


#include <dumux/material/components/co2tablereader.hh>
#include <dumux/material/components/tabulatedcomponent.hh>


#include <appl/material/binarycoefficients/brine_co2.hh>
#include <appl/material/binarycoefficients/brine_h2.hh>
#include <appl/material/binarycoefficients/co2_h2.hh>


//for Marker
#include <iostream>

namespace Dumux {
    
// include the default tables for CO2
#include <dumux/material/components/co2tables.inc>

    
namespace FluidSystems {

/*!
 * \ingroup Fluidsystems
 * \brief A two-phase (water and air) fluid system
 *        with water, carbon dioxide and hydrogen as components.
 *
 * Replacement: CO2 -> CO2
 *              O2 -> H2
 *              H2O -> Brine
 * This fluidsystem uses tabulated version of water of the IAPWS-formulation.
 *
 * Also remember to initialize tabulated components (FluidSystem::init()), while this
 * is not necessary for non-tabularized ones.
 */

template <class Scalar, 
          class CO2Table,  
        bool useComplexRelations = true, 
        class H2Otype = Components::TabulatedComponent<Components::H2O<Scalar>>,
        class BrineRawComponent = Components::Brine<Scalar, Components::H2O<Scalar> >,
        class Brinetype = Components::TabulatedComponent<BrineRawComponent>>



        
class BRINECO2H2
  
  : public BaseFluidSystem<Scalar, BRINECO2H2<Scalar, CO2Table, useComplexRelations,H2Otype,BrineRawComponent, Brinetype>>
{
    using ThisType = BRINECO2H2<Scalar, CO2Table, useComplexRelations>;
    using Base = BaseFluidSystem<Scalar, ThisType>;

    using IdealGas = Dumux::IdealGas<Scalar>;
    using Constants = Dumux::Constants<Scalar>;
    using TabulatedH2O = Components::TabulatedComponent<Dumux::Components::H2O<Scalar> >;
    // Prüfen!!!!!
    using SimpleCO2 = Dumux::Components::CO2<Scalar,CO2Table>;
    using H2 = Dumux::Components::H2<Scalar>;

    //! The components for pure water
    using H2O = TabulatedH2O;
    
    
    
    

    //! The components for pure co2
    using CO2 = SimpleCO2;

    //! The binary coefficients -> necessary in case of template at head (BinaryCoeff)
    using Brine_CO2 = BinaryCoeff::Brine_CO2<Scalar, CO2Table>;
    using CO2_H2 = BinaryCoeff::CO2_H2<Scalar, CO2Table>;
    using Brine_H2= BinaryCoeff::Brine_H2<Scalar>;

public:
    
    
    //! The components for brine
    using Brine = Brinetype;
    
    
    static constexpr int numPhases = 2; //!< Number of phases in the fluid system
    static constexpr int numComponents = 3; //!< Number of components in the fluid system
    static constexpr int numSPhases = 0; // TODO: Remove

    static constexpr int liquidPhaseIdx = 0; //!< index of the liquid phase
    static constexpr int gasPhaseIdx = 1; //!< index of the gas phase
    static constexpr int phase0Idx = liquidPhaseIdx; //!< index of the first phase
    static constexpr int phase1Idx = gasPhaseIdx; //!< index of the second phase

    
    static constexpr int BrineIdx = 0;
    static constexpr int CO2Idx = 1;
    static constexpr int H2Idx = 2;


    static constexpr int comp0Idx = BrineIdx; // first major component
    static constexpr int comp1Idx = CO2Idx; // second major component
    static constexpr int comp2Idx = H2Idx; // secondary component

    // main component at 20°C and 1 bar
    static constexpr int liquidPhaseMainCompIdx = BrineIdx;
    static constexpr int gasPhaseMainCompIdx = CO2Idx;

    /****************************************
     * Fluid phase related static parameters
     ****************************************/
    /*!
     * \brief Return the human readable name of a fluid phase
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static const std::string& phaseName(int phaseIdx)
    {
        static const std::string name[] = {
            std::string("l"),
            std::string("g")
        };

        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return name[phaseIdx];
    }

    /*!
     * \brief Return whether a phase is liquid
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isLiquid(int phaseIdx)
    {
        
        
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return phaseIdx != gasPhaseIdx;
    
    }
    
    
        /*!
     * \brief Return whether a phase is gaseous
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isGas(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return phaseIdx == gasPhaseIdx;
    }
    
    

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are independent on the fluid composition. This assumption is true
     * if Henry's law and Raoult's law apply. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealMixture(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        // we assume Henry's and Raoult's laws for the water phase and
        // and no interaction between gas molecules of different
        // components, so all phases are ideal mixtures!
        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be compressible.
     *
     * Compressible means that the partial derivative of the density
     * to the fluid pressure is always larger than zero.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isCompressible(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        // gases are always compressible
        if (phaseIdx == gasPhaseIdx)
            return true;
        // the water component decides for the liquid phase...
        return Brine::liquidIsCompressible();
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal gas.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealGas(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        if (phaseIdx == gasPhaseIdx)
            // let the components decide
            return Brine::gasIsIdeal() && CO2::gasIsIdeal() && H2::gasIsIdeal();
        return false; // not a gas
    }
    
        /*!
     * \brief Returns whether the fluids are miscible
     */
    static constexpr bool isMiscible()
    { return true; }


    /****************************************
     * Component related static parameters
     ****************************************/
    /*!
     * \brief Return the human readable name of a component
     *
     * \param compIdx The index of the component to consider
     */
    static std::string componentName(int compIdx)
    {
        switch (compIdx)
        {
            case BrineIdx: return Brine::name();
            case CO2Idx: return CO2::name();
            case H2Idx: return H2::name();
        }

        DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
    }

    /*!
     * \brief Return the molar mass of a component in \f$\mathrm{[kg/mol]}\f$.
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar molarMass(int compIdx)
    {
        static const Scalar M[] = {
            Brine::molarMass(),
            CO2::molarMass(),
            H2::molarMass()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return M[compIdx];
    }

    /*!
     * \brief Critical temperature of a component \f$\mathrm{[K]}\f$.
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar criticalTemperature(int compIdx)
    {
        static const Scalar Tcrit[] = {
            Brine::criticalTemperature(),
            CO2::criticalTemperature(),
            H2::criticalTemperature()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return Tcrit[compIdx];
    }

    /*!
     * \brief Critical pressure of a component \f$\mathrm{[Pa]}\f$.
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar criticalPressure(int compIdx)
    {
        static const Scalar pcrit[] = {
            Brine::criticalPressure(),
            CO2::criticalPressure(),
            H2::criticalPressure()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return pcrit[compIdx];
    }

    /*!
     * \brief Molar volume of a component at the critical point \f$\mathrm{[m^3/mol]}\f$.
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar criticalMolarVolume(int compIdx)
    {
        DUNE_THROW(Dune::NotImplemented,
                   "BRINECO2H2FluidSystem::criticalMolarVolume()");
    }

    /*!
     * \brief The acentric factor of a component \f$\mathrm{[-]}\f$.
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar acentricFactor(int compIdx)
    {
        static const Scalar accFac[] = {
            Brine::acentricFactor(),
            CO2::acentricFactor(),
            H2::acentricFactor()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return accFac[compIdx];
    }

    /*!
     * \brief Kelvin equation in \f$\mathrm{[Pa]}\f$
     *
     * Calculate the increase vapor pressure over the
     * curved surface of a drop with radius r
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     * \param radius The radius of the drop
     */
    template <class FluidState>
    static Scalar kelvinVaporPressure(const FluidState &fluidState,
                                      const int phaseIdx,
                                      const int compIdx,
                                      const Scalar radius)
    {
        assert(0 <= phaseIdx  && phaseIdx == liquidPhaseIdx);
        assert(0 <= compIdx  && compIdx == liquidPhaseMainCompIdx);

        Scalar T = fluidState.temperature(phaseIdx);

        //Scalar vaporPressure = H2O::vaporPressure(T);
        Scalar vaporPressure = Brine::vaporPressure(T);
        Scalar exponent = molarMass(compIdx)/(density(fluidState, phaseIdx) * Constants::R * T);
        exponent *= (2 * surfaceTension(fluidState) / radius);
        using std::exp;
        Scalar kelvinVaporPressure = vaporPressure * exp(exponent);

        return kelvinVaporPressure;
    }

    /*!
     * \brief Vapor pressure including the Kelvin equation in \f$\mathrm{[Pa]}\f$
     *
     * Calculate the decreased vapor pressure due to capillarity
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     */
    template <class FluidState>
    static Scalar kelvinVaporPressure(const FluidState &fluidState,
                                      const int phaseIdx,
                                      const int compIdx)
    {
        assert(compIdx == liquidPhaseMainCompIdx && phaseIdx == liquidPhaseIdx);

        using std::exp;
        return fugacityCoefficient(fluidState, phaseIdx, compIdx)
               * fluidState.pressure(phaseIdx)
               * exp(-(fluidState.pressure(gasPhaseIdx)-fluidState.pressure(liquidPhaseIdx))
                          / density(fluidState, phaseIdx)
                          / (Dumux::Constants<Scalar>::R / molarMass(compIdx))
                          / fluidState.temperature());
    }

    /*!
     * \brief Calculate the surface tension between water and air in \f$\mathrm{[\frac{N}{m}]}\f$,
     * according to IAPWS Release on Surface Tension from September 1994.
     * The equation is valid between the triple Point (0.01C) and the critical temperature.
     *
     * \param fluidState An abitrary fluid state
     */
    template <class FluidState>
    static Scalar surfaceTension(const FluidState &fluidState)
    {
        const Scalar T = fluidState.temperature(); //K
        const Scalar B   = 0.2358 ; // [N/m]
        const Scalar T_c = Brine::criticalTemperature(); //K
        const Scalar mu  = 1.256;
        const Scalar b   = -0.625;
        //Equation to calculate surface Tension of Water According to IAPWS Release on Surface Tension from September 1994
        using std::pow;
        const Scalar surfaceTension = B*pow((1.-(T/T_c)),mu)*(1.+b*(1.-(T/T_c)));
        return surfaceTension; //surface Tension [N/m]
    }
    /****************************************
     * thermodynamic relations
     ****************************************/

    /*!
     * \brief Initialize the fluid system's static parameters generically
     *
     * If a tabulated H2O/Brine component is used, we do our best to create
     * tables that always work.
     */
    static void init()
    {
        init(/*tempMin=*/273.15,
             /*tempMax=*/623.15,
             /*numTemp=*/100,
             /*pMin=*/0.0,
             /*pMax=*/20e6,
             /*numP=*/200);
    }

    /*!
     * \brief Initialize the fluid system's static parameters using
     *        problem specific temperature and pressure ranges
     *
     * \param tempMin The minimum temperature used for tabulation of water \f$\mathrm{[K]}\f$
     * \param tempMax The maximum temperature used for tabulation of water \f$\mathrm{[K]}\f$
     * \param nTemp The number of ticks on the temperature axis of the  table of water
     * \param pressMin The minimum pressure used for tabulation of water \f$\mathrm{[Pa]}\f$
     * \param pressMax The maximum pressure used for tabulation of water \f$\mathrm{[Pa]}\f$
     * \param nPress The number of ticks on the pressure axis of the  table of water
     */
    static void init(Scalar tempMin, Scalar tempMax, unsigned nTemp,
                     Scalar pressMin, Scalar pressMax, unsigned nPress)
    {
        if (useComplexRelations)
            std::cout << "Using complex BRINE-CO2-H2 fluid system\n";
        else
            std::cout << "Using fast BRINE-CO2-H2 fluid system\n";

        if (H2O::isTabulated) {
            std::cout << "Initializing tables for the H2O fluid properties ("
                      << nTemp*nPress
                      << " entries).\n";

            TabulatedH2O::init(tempMin, tempMax, nTemp,
                               pressMin, pressMax, nPress);
        }
    

        if(Brine::isTabulated)
        {
            std::cout << "Initializing tables for the brine fluid properties.\n";
            Brine::init(tempMin, tempMax, nTemp,
                                  pressMin, pressMax, nPress);
        }

        
    }

    using Base::density;
    /*!
     * \brief Given a phase's composition, temperature, pressure, and
     *        the partial pressures of all components, return its
     *        density \f$\mathrm{[kg/m^3]}\f$.
     *
     * If useComplexRelations == true, we apply Eq. (7)
     * in Class et al. (2002a) \cite A3:class:2002b <BR>
     * for the liquid density.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    template <class FluidState>
    static Scalar density(const FluidState &fluidState,
                          int phaseIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);

        Scalar sumMoleFrac = 0;
        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
            sumMoleFrac += fluidState.moleFraction(phaseIdx, compIdx);

        // liquid phase
        if (phaseIdx == liquidPhaseIdx) {
            if (!useComplexRelations)
                // assume pure water
                return Brine::liquidDensity(T, p);
            else
            {
                // See: Eq. (7) in Class et al. (2002a)
                Scalar rholBrine = Brine::liquidDensity(T,p);
                Scalar clBrine = rholBrine/Brine::molarMass();

                // this assumes each nitrogen molecule displaces exactly one
                // water molecule in the liquid
                return
                    clBrine*
                    (fluidState.moleFraction(liquidPhaseIdx, BrineIdx)*Brine::molarMass()
                    +
                    fluidState.moleFraction(liquidPhaseIdx, CO2Idx)*CO2::molarMass()
                    +
                    fluidState.moleFraction(liquidPhaseIdx, H2Idx)*H2::molarMass())
                    / sumMoleFrac;
            }
        }

      // gas phase
        using std::max;
        if (!useComplexRelations)
            // for the gas phase assume an ideal gas
            return IdealGas::molarDensity(T, p)
                   * fluidState.averageMolarMass(gasPhaseIdx);

        // assume ideal mixture: steam, brine, co2 and hydrogen don't "see" each
        // other
        return Brine::gasDensity(T, fluidState.partialPressure(gasPhaseIdx, BrineIdx))
               + H2::gasDensity(T, fluidState.partialPressure(gasPhaseIdx, H2Idx))
               + CO2::gasDensity(T, fluidState.partialPressure(gasPhaseIdx, CO2Idx));
    }

    using Base::molarDensity;
    /*!
     * \brief The molar density \f$\rho_{mol,\alpha}\f$
     *   of a fluid phase \f$\alpha\f$ in \f$\mathrm{[mol/m^3]}\f$
     *
     * The molar density for the simple relation is defined by the
     * mass density \f$\rho_\alpha\f$ and the molar mass of the main component
     *
     * The molar density for the complrex relation is defined by the
     * mass density \f$\rho_\alpha\f$ and the mean molar mass \f$\overline M_\alpha\f$:
     *
     * \f[\rho_{mol,\alpha} = \frac{\rho_\alpha}{\overline M_\alpha} \;.\f]
     */
    template <class FluidState>
    static Scalar molarDensity(const FluidState &fluidState, int phaseIdx)
    {
        const Scalar T = fluidState.temperature(phaseIdx);
        const Scalar p = fluidState.pressure(phaseIdx);

        if (phaseIdx == liquidPhaseIdx)
        {
            // assume pure water or that each gas molecule displaces exactly one
            // molecule in the liquid.
            return Brine::liquidMolarDensity(T, p);
        }
        else
        {
            if (!useComplexRelations)
            {   //assume ideal gas
                return IdealGas::molarDensity(T,p);
            }

            return Brine::gasMolarDensity(T, fluidState.partialPressure(gasPhaseIdx, BrineIdx))
                   + H2::gasMolarDensity(T, fluidState.partialPressure(gasPhaseIdx, H2Idx))
                   + CO2::gasMolarDensity(T, fluidState.partialPressure(gasPhaseIdx, CO2Idx));
        }
    }

    using Base::viscosity;
    /*!
     * \brief Calculate the dynamic viscosity of a fluid phase \f$\mathrm{[Pa*s]}\f$
     *
     * Compositional effects in the gas phase are accounted by the Wilke method.
     * See Reid et al. (1987)  \cite reid1987 <BR>
     * 4th edition, McGraw-Hill, 1987, 407-410
     * 5th edition, McGraw-Hill, 20001, p. 9.21/22
     * \note Compositional effects for a liquid mixture have to be implemented.
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState,
                            int phaseIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);

        // liquid phase
        if (phaseIdx == liquidPhaseIdx) {
            // assume pure water for the liquid phase
            return Brine::liquidViscosity(T, p);
        }

        // gas phase
        if (!useComplexRelations)
        {
            // assume pure carbon dioxide for the gas phase
            return CO2::gasViscosity(T,p);

        }
        else
        {
            // Wilke method (Reid et al.):
            Scalar muResult = 0;
            const Scalar mu[numComponents] = {
                Brine::gasViscosity(T, Brine::vaporPressure(T)),
                CO2::gasViscosity(T, p),
                H2::gasViscosity(T, p)

            };

            Scalar sumx = 0.0;
            using std::max;
            for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                sumx += fluidState.moleFraction(phaseIdx, compIdx);
            sumx = max(1e-10, sumx);

            for (int i = 0; i < numComponents; ++i) {
                Scalar divisor = 0;
                using std::pow;
                using std::sqrt;
                for (int j = 0; j < numComponents; ++j) {
                    Scalar phiIJ = 1 + sqrt(mu[i]/mu[j]) * pow(molarMass(j)/molarMass(i), 1/4.0);
                    phiIJ *= phiIJ;
                    phiIJ /= sqrt(8*(1 + molarMass(i)/molarMass(j)));
                    divisor += fluidState.moleFraction(phaseIdx, j)/sumx * phiIJ;
                }
                muResult += fluidState.moleFraction(phaseIdx, i)/sumx * mu[i] / divisor;
            }
            return muResult;
        }
    }

    using Base::fugacityCoefficient;
    /*!
     * \brief Returns the fugacity coefficient \f$\mathrm{[-]}\f$ of a component in a
     *        phase.
     *
     * The fugacity coefficient \f$\phi^\kappa_\alpha\f$ of
     * component \f$\kappa\f$ in phase \f$\alpha\f$ is connected to
     * the fugacity \f$f^\kappa_\alpha\f$ and the component's mole
     * fraction \f$x^\kappa_\alpha\f$ by means of the relation
     *
     * \f[
     f^\kappa_\alpha = \phi^\kappa_\alpha\;x^\kappa_\alpha\;p_\alpha
     \f]
     * where \f$p_\alpha\f$ is the pressure of the fluid phase.
     *
     * For liquids with very low miscibility this boils down to the
     * Henry constant for the solutes and the saturated vapor pressure
     * both divided by phase pressure.
     */
    template <class FluidState>
    static Scalar fugacityCoefficient(const FluidState &fluidState,
                                      int phaseIdx,
                                      int compIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);
        assert(0 <= compIdx  && compIdx < numComponents);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);

        Scalar pl = fluidState.pressure(liquidPhaseIdx);
        Scalar pg = fluidState.pressure(gasPhaseIdx);

         // calulate the equilibrium composition for the given
        // temperature and pressure.
        Scalar xlH2O, xgH2O;
        Scalar xlCO2, xgCO2;
        Brine_CO2::calculateMoleFractions(T,
                                          pl,
                                         0.2 /*BrineRawComponent::salinity*/,
                                          /*knowgasPhaseIdx=*/-1,
                                          xlCO2,
                                          xgH2O);

        // normalize the phase compositions
        using std::min;
        using std::max;
        xlCO2 = max(0.0, min(1.0, xlCO2));
        xgH2O = max(0.0, min(1.0, xgH2O));

        xlH2O = 1.0 - xlCO2;
        xgCO2 = 1.0 - xgH2O;

        //Scalar phigCO2 = 1.0;
        // liquid phase
        if (phaseIdx == liquidPhaseIdx)
        {
            switch(compIdx){
            case BrineIdx: return (xgH2O/xlH2O)*(pg/pl);    /*gBrine::vaporPressure(T)/p;*/
            case CO2Idx: return (xgCO2/xlCO2)*(pg/pl); /*Brine_CO2::henry(T)/p;*/
            case H2Idx: return Brine_H2::henry(T)/p;
            };
        }

        // for the gas phase, assume an ideal gas when it comes to
        // fugacity (-> fugacity == partial pressure)
        return 1.0;
    }

    

    using Base::diffusionCoefficient;
    /*!
     * \brief Calculate the molecular diffusion coefficient for a
     *        component in a fluid phase \f$\mathrm{[mol^2 * s / (kg*m^3)]}\f$
     *
     * Molecular diffusion of a compoent \f$\kappa\f$ is caused by a
     * gradient of the chemical potential and follows the law
     *
     * \f[ J = - D \mathbf{grad} \mu_\kappa \f]
     *
     * where \f$\mu_\kappa\f$ is the component's chemical potential,
     * \f$D\f$ is the diffusion coefficient and \f$J\f$ is the
     * diffusive flux. \f$mu_\kappa\f$ is connected to the component's
     * fugacity \f$f_\kappa\f$ by the relation
     *
     * \f[ \mu_\kappa = R T_\alpha \mathrm{ln} \frac{f_\kappa}{p_\alpha} \f]
     *
     * where \f$p_\alpha\f$ and \f$T_\alpha\f$ are the fluid phase'
     * pressure and temperature.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     */
    template <class FluidState>
    static Scalar diffusionCoefficient(const FluidState &fluidState,
                                       int phaseIdx,
                                       int compIdx)
    {
        DUNE_THROW(Dune::NotImplemented, "Diffusion coefficients");
    }

    using Base::binaryDiffusionCoefficient;
    /*!
     * \brief Given a phase's composition, temperature and pressure,
     *        return the binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for components
     *        \f$i\f$ and \f$j\f$ in this phase.
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIIdx The index of the first component to consider
     * \param compJIdx The index of the second component to consider
     */
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                             int phaseIdx,
                                             int compIIdx,
                                             int compJIdx)

    {
        if (compIIdx > compJIdx)
        {
            using std::swap;
            swap(compIIdx, compJIdx);
        }

#ifndef NDEBUG
        if (compIIdx == compJIdx ||
            phaseIdx > numPhases - 1 ||
            compJIdx > numComponents - 1)
        {
            DUNE_THROW(Dune::InvalidStateException,
                       "Binary diffusion coefficient of components "
                       << compIIdx << " and " << compJIdx
                       << " in phase " << phaseIdx << " is undefined!\n");
        }
#endif

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);

        // liquid phase
        if (phaseIdx == liquidPhaseIdx) {
            if (compIIdx == BrineIdx && compJIdx == CO2Idx)
                return Brine_CO2::liquidDiffCoeff(T, p);
            if (compIIdx == BrineIdx && compJIdx == H2Idx)
                return Brine_H2::liquidDiffCoeff(T, p);
            DUNE_THROW(Dune::InvalidStateException,
                       "Binary diffusion coefficient of components "
                       << compIIdx << " and " << compJIdx
                       << " in phase " << phaseIdx << " is undefined!\n");
        }
        // gas phase
        if (phaseIdx == gasPhaseIdx) {
            if (compIIdx == BrineIdx && compJIdx == CO2Idx)
                return Brine_CO2::gasDiffCoeff(T, p);
            if (compIIdx == BrineIdx && compJIdx == H2Idx)
                return Brine_H2::gasDiffCoeff(T, p);
            if(compIIdx == CO2Idx && compJIdx == H2Idx)
                return CO2_H2::gasDiffCoeff(T, p);
            DUNE_THROW(Dune::InvalidStateException,
                       "Binary diffusion coefficient of components "
                       << compIIdx << " and " << compJIdx
                       << " in phase " << phaseIdx << " is undefined!\n");
        }

        DUNE_THROW(Dune::InvalidStateException,
                  "Binary diffusion coefficient of components "
                  << compIIdx << " and " << compJIdx
                  << " in phase " << phaseIdx << " is undefined!\n");
    }

    using Base::enthalpy;
    /*!
     * \brief Given a phase's composition, temperature, pressure and
     *        density, calculate its specific enthalpy \f$\mathrm{[J/kg]}\f$.
     *
     *  \note This fluid system neglects the contribution of
     *        gas-molecules in the liquid phase. This contribution is
     *        probably not big. Somebody would have to find out the
     *        enthalpy of solution for this system. ...
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    template <class FluidState>
    static Scalar enthalpy(const FluidState &fluidState,
                           int phaseIdx)
    {
        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);
        Valgrind::CheckDefined(T);
        Valgrind::CheckDefined(p);

        // liquid phase
        if (phaseIdx == liquidPhaseIdx) {
            
            //Caution should be implemented for binary combination of hydrogen and co2
            Scalar XlCO2 = fluidState.massFraction(phaseIdx, CO2Idx);
            Scalar result = liquidEnthalpyBrineCO2_(T,
                                                    p,
                                                    BrineRawComponent::constantSalinity,
                                                    XlCO2);
            
            Valgrind::CheckDefined(result);
            return result;
           
        }
        // gas phase
        else if (phaseIdx == gasPhaseIdx)
        {
            // assume ideal mixture: which means
            // that the total specific enthalpy is the sum of the
            // "partial specific enthalpies" of the components.
            Scalar hBrine =
                fluidState.massFraction(gasPhaseIdx, BrineIdx)
                * Brine::gasEnthalpy(T, p);
            Scalar hCO2 =
                fluidState.massFraction(gasPhaseIdx, CO2Idx)
                * CO2::gasEnthalpy(T,p);
            Scalar hH2 =
                fluidState.massFraction(gasPhaseIdx, H2Idx)
                * H2::gasEnthalpy(T,p);
            return hBrine + hCO2 + hH2;
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }
    /*!
     * \brief Returns the specific enthalpy \f$\mathrm{[J/kg]}\f$ of a component in a specific phase
     */
    template <class FluidState>
    static Scalar componentEnthalpy(const FluidState &fluidState,
                                    int phaseIdx,
                                    int componentIdx)
    {
        DUNE_THROW(Dune::NotImplemented, "Component enthalpies");
    }
    
    
    static Scalar liquidEnthalpyBrineCO2_(Scalar T,
                                          Scalar p,
                                          Scalar S,
                                          Scalar X_CO2_w)
    {
        /* X_CO2_w : mass fraction of CO2 in brine */
    
        const Scalar h_ls1 = BrineRawComponent::liquidEnthalpy(T, p, S)/1E3; /* J/kg */

        /* heat of dissolution for CO2 according to Fig. 6 in Duan and Sun 2003. (kJ/kg)
           In the relevant temperature ranges CO2 dissolution is
           exothermal */
        const Scalar delta_hCO2 = (-57.4375 + T * 0.1325) * 1000/44;

        const Scalar hw = H2O::liquidEnthalpy(T, p) /1E3; /* kJ/kg */

        /* enthalpy contribution of CO2 (kJ/kg) */
        const Scalar hg = CO2::liquidEnthalpy(T, p)/1E3 + delta_hCO2;

        /* Enthalpy of brine with dissolved CO2 */
        const Scalar h_ls = (h_ls1 - X_CO2_w*hw + hg*X_CO2_w)*1E3; /*J/kg*/

        return h_ls;
    }
    
    
    
    
    
    
    

    using Base::thermalConductivity;
    /*!
     * \brief Thermal conductivity of a fluid phase \f$\mathrm{[W/(m K)]}\f$.
     *
     * Use the conductivity of air and water as a first approximation.
     *
     * http://en.wikipedia.org/wiki/List_of_thermal_conductivities
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    template <class FluidState>
    static Scalar thermalConductivity(const FluidState &fluidState,
                                      const int phaseIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);
        Scalar temperature  = fluidState.temperature(phaseIdx) ;
        Scalar pressure = fluidState.pressure(phaseIdx);

        if (phaseIdx == liquidPhaseIdx)
        {
            return Brine::liquidThermalConductivity(temperature, pressure);
        }
        else
        {
            Scalar lambdaPureCO2 = CO2::gasThermalConductivity(temperature, pressure);
            Scalar lambdaPureH2 = H2::gasThermalConductivity(temperature, pressure);
            if (useComplexRelations)
            {
                Scalar xCO2 = fluidState.moleFraction(phaseIdx, CO2Idx);
                Scalar xH2 = fluidState.moleFraction(phaseIdx, H2Idx);
                Scalar xBrine = fluidState.moleFraction(phaseIdx, BrineIdx);
                Scalar lambdaCO2 = xCO2 * lambdaPureCO2;
                Scalar lambdaH2 = xH2 * lambdaPureH2;
                Scalar partialPressure  = pressure * xBrine;
                Scalar lambdaBrine = xBrine * Brine::gasThermalConductivity(temperature, partialPressure);
                return lambdaCO2 + lambdaBrine + lambdaH2;
            }
            else
                return lambdaPureCO2;
        }
    }

    using Base::heatCapacity;
    /*!
     * \brief Specific isobaric heat capacity of a fluid phase.
     *        \f$\mathrm{[J/kg*K]}\f$.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    template <class FluidState>
    static Scalar heatCapacity(const FluidState &fluidState,
                               int phaseIdx)
    {
        if (phaseIdx == liquidPhaseIdx) {
            return Brine::liquidHeatCapacity(fluidState.temperature(phaseIdx),
                                           fluidState.pressure(phaseIdx));
        }

        Scalar c_pCO2;
        Scalar c_pH2;
        Scalar c_pBrine;
        // let the  brine, hydrogen, co2 components do things their own way
        if (useComplexRelations) {
            c_pCO2 = CO2::gasHeatCapacity(fluidState.temperature(phaseIdx),
                                        fluidState.pressure(phaseIdx)
                                        * fluidState.moleFraction(phaseIdx, CO2Idx));

            c_pBrine = Brine::gasHeatCapacity(fluidState.temperature(phaseIdx),
                                          fluidState.pressure(phaseIdx)
                                          * fluidState.moleFraction(phaseIdx, BrineIdx));
            c_pH2 = H2::gasHeatCapacity(fluidState.temperature(phaseIdx),
                                          fluidState.pressure(phaseIdx)
                                          * fluidState.moleFraction(phaseIdx, H2Idx));
        }
        else {
            // assume an ideal gas for both components. See:
            
            
            //https://webbook.nist.gov/chemistry/fluid/
            //Cv,m= 20.520 J/mol K -> H2
            //Cv,m= 28.928 J/mol K -> CO2
            
            
           
            Scalar c_vCO2molar = Constants::R*3.42;
            Scalar c_pCO2molar = Constants::R + c_vCO2molar;

            Scalar c_vH2molar = Constants::R*2.43;
            Scalar c_pH2molar = Constants::R + c_vH2molar;

            Scalar c_vBrinemolar = Constants::R*3.37; // <- correct??
            Scalar c_pBrinemolar = Constants::R + c_vBrinemolar;

             ////AAAAAACCHHTTTTUNNGGGG DATEN ÄNDERN!!!!! ->fehlt
            c_pCO2 = c_pCO2molar/molarMass(CO2Idx);
            c_pH2 = c_pH2molar/molarMass(H2Idx);
            c_pBrine = c_pBrinemolar/molarMass(BrineIdx);
        }

        // mangle all components together
        return
            c_pBrine*fluidState.massFraction(gasPhaseIdx, BrineIdx)
            + c_pCO2*fluidState.massFraction(gasPhaseIdx, CO2Idx)
            + c_pH2*fluidState.massFraction(gasPhaseIdx, H2Idx);
    }

};

} // end namespace FluidSystems
} // end namespace Dumux

#endif