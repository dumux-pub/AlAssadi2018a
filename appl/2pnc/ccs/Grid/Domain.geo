size = 1;
b = 1400;
h = 400;
offset = 40;

numElementsX = 200;
numElementsY = 200;
refX = 1.0;
refY = 1.0;


numElementsX2 = 2;
numElementsY2 = 2;

Point(1) = {0, 0, 0, size};
Point(2) = {b, offset*2, 0, size};
Point(3) = {b, offset+h, 0, size};
Point(4) = {0, h, 0, size};


// AAA: Stretching and changing x-y- Values

Point(5) = {b*0.65, offset*1.7, 0 , size};
Point(6) = {(b*0.60), offset+h*1.3, 0 , size};

// AAA: Inserting 2 Points 


Point(7) = {b*0.35, offset, 0 , size};
Point(8) = {(b*0.35), offset+h*1.05, 0 , size};

// AAA: Inserting 2 Points 


Point(9) = {b*0.80, offset*1.85, 0 , size};
Point(10) = {(b*0.9), offset+h*1.05, 0 , size};











// outline left/right Main
Line(3) = {1, 4};
Line(4) = {2, 3};

// AAA: Connecting all points top

Spline(1) = {1, 7, 5, 9, 2};
Spline(2) = {4, 8, 6, 10, 3};






// AAA: x-discretization central 
Transfinite Line{1, 2} = numElementsX Using Progression refX;
Transfinite Line{3, 4} = numElementsY Using Progression refY;









// AAA: surface central
Line Loop(5) = {3, 2, -4, -1};
Plane Surface(6) = {5};






// AAA: central
Transfinite Surface{6};
Recombine Surface{6};




