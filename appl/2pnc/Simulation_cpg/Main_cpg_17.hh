// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCTest
 * \brief Problem where carbon dioxide and hydrogen are injected in a reservoir 
 */
#ifndef DUMUX_TWOPNCNI_CPG_PROBLEM_HH
#define DUMUX_TWOPNCNI_CPG_PROBLEM_HH

#include <dumux/discretization/cellcentered/tpfa/properties.hh>
#include <dumux/discretization/box/properties.hh>
#include <dumux/porousmediumflow/2pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/discretization/evalgradients.hh>
#include <appl/material/fluidsystems/brineco2h2experiment.hh>
#include "Main_spatialparams.hh"
#include <appl/2pnc/ccs/co2tables.hh>
#include <dune/grid/uggrid.hh>
#include <stdlib.h>  
#include <dumux/discretization/maxwellstefanslaw.hh>

#ifndef DIFFUSIONTYPE // default to Fick's law if not set through CMake
#define DIFFUSIONTYPE FicksLaw<TypeTag>
#endif

namespace Dumux {
  
    
template <class TypeTag>
class TwoPNCNICPGProblem;

namespace Properties {
//Non-Isothermal     
NEW_TYPE_TAG(TwoPNCNICPGTypeTag, INHERITS_FROM(TwoPNCNI));

NEW_TYPE_TAG(TwoPNCNICPGCCTypeTag, INHERITS_FROM(CCTpfaModel, TwoPNCNICPGTypeTag));

// Set the grid type
SET_TYPE_PROP(TwoPNCNICPGTypeTag, Grid, Dune::UGGrid<2>);
// SET_TYPE_PROP(TwoPNCNICPGTypeTag, Grid, Dune::YaspGrid<2>);
// Set the problem property
SET_TYPE_PROP(TwoPNCNICPGTypeTag, Problem, TwoPNCNICPGProblem<TypeTag>);

//Set fluid configuration
SET_PROP(TwoPNCNICPGTypeTag, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    static const bool useComplexRelations = true;
public:
    using type = FluidSystems::BRINECO2H2<Scalar, CO2Tables, useComplexRelations>;
};

// Set the spatial parameters
SET_TYPE_PROP(TwoPNCNICPGTypeTag, SpatialParams, TwoPNCNICPGSpatialParams<TypeTag>);

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(TwoPNCNICPGTypeTag, UseMoles, true);

//! Here we set FicksLaw or TwoPNCDiffusionsLaw
SET_TYPE_PROP(TwoPNCNICPGTypeTag, MolecularDiffusionType, DIFFUSIONTYPE);

//! Set the default formulation to pw-Sn: This can be over written in the problem.
SET_PROP(TwoPNCNICPGTypeTag, Formulation)
{ static constexpr auto value = TwoPFormulation::p0s1; };


} // end namespace Properties

/*!
 * \ingroup TwoPNCTest
 * \brief DOC_ME
 */
template <class TypeTag>
class TwoPNCNICPGProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Intersection = typename GET_PROP_TYPE(TypeTag, GridView)::Intersection;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using FluidState = typename GET_PROP_TYPE(TypeTag, FluidState) ;
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using ModelTraits = typename GET_PROP_TYPE(TypeTag, ModelTraits);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using ParameterCache = typename FluidSystem::ParameterCache;
   
    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
        
        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
        
        // FluidSystem
        CO2Idx = FluidSystem::CO2Idx,
        H2Idx = FluidSystem::H2Idx,
        BrineIdx = FluidSystem::BrineIdx,
        

        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,

        
        // equation indices
        contiCO2EqIdx =     Indices::conti0EqIdx + CO2Idx,
        contiH2EqIdx  =     Indices::conti0EqIdx + H2Idx,
        contiBrineEqIdx =   Indices::conti0EqIdx + BrineIdx,
        
        
        // phase presence index
        firstPhaseOnly = Indices::firstPhaseOnly,
        secondPhaseOnly = Indices::secondPhaseOnly,
        bothPhases = Indices::bothPhases,
        
        // wetting/non wetting
        liquidPhaseIdx = FluidSystem::liquidPhaseIdx,
        gasPhaseIdx = FluidSystem::gasPhaseIdx,
    
    };

   
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    
    
    //! property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
    static const bool isBox = GET_PROP_TYPE(TypeTag, FVGridGeometry)::discMethod == DiscretizationMethod::box;
    


public:
    /*!
     * \brief The constructor
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    TwoPNCNICPGProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        // initialize the tables of the fluid system
        FluidSystem::init();

        name_               = getParam<std::string>("Problem.Name");
        injectionDuration_  = getParam<Scalar>("Source.InjectionDuration");
        co2Temperature_     = getParam<Scalar>("Source.CO2Temperature");
        co2Pressure_        = getParam<Scalar>("Source.CO2Pressure");
        co2Pressurepro_        = getParam<Scalar>("Source.CO2Pressure_pro");
        h2Temperature_      = getParam<Scalar>("Source.H2Temperature");
        h2Pressure_         = getParam<Scalar>("Source.H2Pressure");
        h2Pressurepro_         = getParam<Scalar>("Source.H2Pressure_pro");
        reservoirBottom_    = getParam<Scalar>("Reservoir.ReservoirBottom");
        surfacePressure_    = getParam<Scalar>("Surface.SurfacePressure");
        surfaceTemperature_ = getParam<Scalar>("Surface.SurfaceTemperature");
        h2X_                = getParam<Scalar>("Source.h2X");
        permeability_       = getParam<Scalar>("Reservoir.PermeabilityReservoir");
                            
        unsigned int codim = GET_PROP_TYPE(TypeTag, FVGridGeometry)::discMethod == DiscretizationMethod::box ? dim : 0;
        temperature_.resize(fvGridGeometry->gridView().size(codim));

        //stateing in the console whether mole or mass fractions are used
        if(useMoles)
        {
            std::cout<<"problem uses mole-fractions"<<std::endl;
        }
        else
        {
            std::cout<<"problem uses mass-fractions"<<std::endl;
        }
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the problem name
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature [K]
     */
    Scalar temperatureAtPos(const GlobalPosition &globalPos) const
    { return initialTemperatureField_(globalPos); }
    
   

    const std::vector<Scalar>& getTemperature()
    { return temperature_; }
   
    
    
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position for which the bc type should be evaluated
     */
     BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
     {
         BoundaryTypes values;

         //right side & left side
          if(globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_ ||globalPos[0] < eps_ )
          {   values.setAllDirichlet();}
          
        
         //top & bottom side
         else
         {
             values.setAllNeumann();
         }
         return values;
     }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param values Stores the Dirichlet values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} ] \f$
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        
        return initial_(globalPos);
    }

 
 
   
    /*!
     * \brief Evaluate the boundary conditions for a Neumann
     *        boundary segment.
     *
     * For this method, the \a priVars parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^2*s) or kg/(m^2*s))
     */
        
    PrimaryVariables neumannAtPos(const GlobalPosition &globalPos) const
    {
        // initialize values to zero, i.e. no-flow Neumann boundary conditions
        PrimaryVariables values(0.0);        

        return values;
    }


 /*!
     * \brief Evaluate the source term for all balance equations within a given
     *        sub-control-volume.
     *
     * \param values Stores the solution for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} / (m^\textrm{dim} \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     *
     * Positive values mean that mass is created, negative ones mean that it vanishes.
     */
    //! \copydoc Dumux::ImplicitProblem::source()
    
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector values(0.0);
        const auto& globalPos = scv.dofPosition();
        Scalar deltaX= 14;
        Scalar deltaY= 5;
        Scalar r_e= 0.14 * sqrt(deltaX * deltaX + (deltaY *deltaY)); 
        Scalar Wellradius= 0.25;
        
        
        ParameterCache dummyCache;
        FluidState fluidState;
        fluidState.setPressure(gasPhaseIdx,co2Pressure_);
        fluidState.setTemperature(co2Temperature_ );
        const Scalar molarDensity = FluidSystem::molarDensity(fluidState,
                                                              dummyCache,
                                                              gasPhaseIdx);
        
        //https://www.onepetro.org/download/journal-paper/SPE-6893-PA?id=journal-paper%2FSPE-6893-PA
        //[2] http://www.sinmec.ufsc.br/site/arquivos/e-emrlszvcwz_gustavo_g_ribeiro_encit2014.pdf
        //Q_volume_CO2 / calar Q_volume_H2  [2]-> eq. 7
        //Position Well 1
        Scalar X1_injection = 480;
        Scalar X2_injection = 500;
        Scalar Y1_injection = 60;
        Scalar Y2_injection = 300; 
    
        // Reverse Peacemen
        Scalar X1_extraction = 835;
        Scalar X2_extraction = 855;
        Scalar Y1_extraction = 300;
        Scalar Y2_extraction =  this->fvGridGeometry().bBoxMax()[1];

   if(time_ < injectionDuration_  &&  globalPos[0] > X1_injection && globalPos[0] < X2_injection && globalPos[1] > Y1_injection && globalPos[1] < Y2_injection) {
      // if condition is true then print the following
        Scalar depth = 1;
        Scalar V_cell= deltaX * deltaY * depth ;
        
        Scalar Q_volume_CO2 = (1-h2X_)*(((elemVolVars[scv].pressure(gasPhaseIdx) -co2Pressure_  ) * 2.0 * M_PI * permeability_ * deltaY )/(log(r_e/Wellradius) * Components::CO2<Scalar,CO2Tables>::gasViscosity(co2Temperature_,co2Pressure_ ))); 
    

        Scalar Q_volume_H2 =  h2X_* (((elemVolVars[scv].pressure(gasPhaseIdx) -co2Pressure_  ) * 2.0 * M_PI * permeability_ * deltaY )/(log(r_e/Wellradius) * Components::CO2<Scalar,CO2Tables>::gasViscosity(co2Temperature_,co2Pressure_ )));
        
        Scalar Q_mol_CO2 = (Q_volume_CO2 * molarDensity)/ V_cell ;
       
        Scalar Q_mol_H2 = (Q_volume_H2 * molarDensity) / V_cell;    
        if ( Q_volume_H2 < 0 && Q_volume_CO2 < 0 )
        {
        values[contiCO2EqIdx]= -Q_mol_CO2;   
        values[contiH2EqIdx]= -Q_mol_H2 ;
        values[energyEqIdx] = -Q_mol_CO2 *Components::CO2<Scalar,CO2Tables>::molarMass()* Components::CO2<Scalar,CO2Tables>::gasEnthalpy(co2Temperature_, co2Pressure_)+ (-Q_mol_H2 *Components::H2<Scalar>::molarMass() * Components::H2<Scalar>::gasEnthalpy(co2Temperature_, co2Pressure_));
        
        std::cout << Components::CO2<Scalar,CO2Tables>::gasViscosity(co2Temperature_,co2Pressure_ ) << "Viss at Injection well" << "\n"; 
        
        
        }
        else{
             values[contiCO2EqIdx]= 0;   
        values[contiH2EqIdx]= 0;
        values[energyEqIdx]= 0; 
        }
        std::cout << values[contiCO2EqIdx] << "Injection well" << "\n";
        
        
   }    else if (time_ > 0.4*injectionDuration_  && time_ < 2*injectionDuration_   && globalPos[0] > X1_extraction && globalPos[0] < X2_extraction && globalPos[1] > Y1_extraction && globalPos[1] < Y2_extraction &&                   elemVolVars[scv].saturation(gasPhaseIdx)>0) {
      // if else if condition is true
        Scalar depth = 1;
        Scalar V_cell= deltaX * deltaY * depth ;
       
        
        Scalar mobilitygasPhase = std::max(0.1,elemVolVars[scv].mobility(gasPhaseIdx));
        Scalar mobilityliquidphase = std::max(0.1,elemVolVars[scv].mobility(liquidPhaseIdx));
       
        
        
        //gas  CO2
        
        Scalar Q_volume_CO2 =((mobilitygasPhase*elemVolVars[scv].moleFraction(gasPhaseIdx,CO2Idx)*(elemVolVars[scv].pressure(gasPhaseIdx) -co2Pressurepro_ ) * 2.0 * M_PI * permeability_ * deltaY )/(log(r_e/Wellradius)));
       
        
        
        // liquid + gas CO2
        Q_volume_CO2= Q_volume_CO2 + (mobilityliquidphase*elemVolVars[scv].moleFraction(liquidPhaseIdx,CO2Idx)*(((elemVolVars[scv].pressure(liquidPhaseIdx) -co2Pressurepro_  ) * 2.0 * M_PI * permeability_ * deltaY )/(log(r_e/Wellradius))));
        
        
        // gas H2
        
        Scalar Q_volume_H2 =   (((mobilitygasPhase*elemVolVars[scv].moleFraction(gasPhaseIdx,H2Idx)*(elemVolVars[scv].pressure(gasPhaseIdx) -co2Pressurepro_  )) * 2.0 * M_PI * permeability_ * deltaY )/(log(r_e/Wellradius) ));
        
        
        
        // liquid + gas H2
        
       Q_volume_H2 = Q_volume_H2 + ((((mobilityliquidphase*elemVolVars[scv].moleFraction(gasPhaseIdx,H2Idx)* (elemVolVars[scv].pressure(gasPhaseIdx) -co2Pressurepro_  )) * 2.0 * M_PI * permeability_ * deltaY )/(log(r_e/Wellradius) )));
        
        
        // gas Brine
        
        Scalar Q_volume_Brine =  (((mobilitygasPhase*elemVolVars[scv].moleFraction(gasPhaseIdx,BrineIdx)* (elemVolVars[scv].pressure(gasPhaseIdx) -co2Pressurepro_)  ) * 2.0 * M_PI * permeability_ * deltaY )/(log(r_e/Wellradius)));
        
         // liquid + gas Brin
        
         Q_volume_Brine = Q_volume_Brine+ ((((mobilityliquidphase*elemVolVars[scv].moleFraction(gasPhaseIdx,BrineIdx)*(elemVolVars[scv].pressure(gasPhaseIdx) -co2Pressurepro_  )) * 2.0 * M_PI * permeability_ * deltaY )/(log(r_e/Wellradius))));
        
        
        
        Scalar Q_mol_CO2 = (Q_volume_CO2 * molarDensity)/ V_cell ;
       
        Scalar Q_mol_H2 = (Q_volume_H2 * molarDensity) / V_cell; 
        
        Scalar Q_mol_Brine=(Q_volume_Brine * molarDensity) / V_cell; 
        
        
        if ( Q_volume_H2 < 0 && Q_volume_CO2 < 0 )
        {
        Scalar maxfactor = 0.1;
        Q_mol_CO2 = maxfactor * Q_mol_CO2;
        Q_mol_H2= maxfactor * Q_mol_H2;
        Q_mol_Brine= maxfactor * Q_mol_Brine;
        
       
    
        values[contiCO2EqIdx]= Q_mol_CO2;   
        values[contiH2EqIdx]= Q_mol_H2 ;
        values[contiBrineEqIdx]= Q_mol_Brine ;
        //values[contiBrineEqIdxEqIdx] = 
        //should be implimented
 

        std::cout << elemVolVars[scv].moleFraction(gasPhaseIdx,BrineIdx) << "elemVolVars[scv].moleFraction(gasPhaseIdx,BrineIdx) at Production well" << "\n";
        std::cout << elemVolVars[scv].moleFraction(gasPhaseIdx,CO2Idx) << "elemVolVars[scv].moleFraction(gasPhaseIdx,CO2Idx) at Production well" << "\n";
        std::cout << elemVolVars[scv].moleFraction(gasPhaseIdx,H2Idx) << "elemVolVars[scv].moleFraction(gasPhaseIdx,H2Idx) at Production well" << "\n";      
        std::cout << elemVolVars[scv].saturation(gasPhaseIdx) << "elemVolVars[scv].saturation(gasPhaseIdx) at Production well" << "\n";
         std::cout << elemVolVars[scv].pressure(gasPhaseIdx) << "elemVolVars[scv].pressure(gasPhaseIdx) at Production well" << "\n";
        std::cout << Components::CO2<Scalar,CO2Tables>::gasViscosity(elemVolVars[scv].temperature(),co2Pressurepro_ ) << "Viss at Production well" << "\n"; 
        
        
        
        //Caution -> should be implimented via elemVolVars
        
        values[energyEqIdx] = Q_mol_CO2 *Components::CO2<Scalar,CO2Tables>::molarMass()* Components::CO2<Scalar,CO2Tables>::gasEnthalpy(elemVolVars[scv].temperature(), co2Pressurepro_)+ (Q_mol_H2 *Components::H2<Scalar>::molarMass() * Components::H2<Scalar>::gasEnthalpy(elemVolVars[scv].temperature(), co2Pressurepro_));
        }
        else{
             values[contiCO2EqIdx]= 0;   
        values[contiH2EqIdx]= 0;
        values[energyEqIdx]= 0;
        }
       std::cout << values[contiCO2EqIdx] << "Production well" << "\n";
       
       
       
   } else {
      // if none of the conditions is true
      values[contiCO2EqIdx]= 0;   
    values[contiH2EqIdx]= 0 ;
    values[energyEqIdx] = 0;
   }

     return values;  
        
    }
    
    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial values for a control volume
     *
     * \param values Stores the initial values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variables} ] \f$
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        return initial_(globalPos);
    }
    
    
    //! set the time for the time dependent boundary conditions (called from main)
     void setTime(Scalar time)
    { time_ = time; }
    

private:
    /*!
     * \brief Evaluates the initial values for a control volume
     *
     * The internal method for the initial condition
     *
     * \param values Stores the initial values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variables} ] \f$
     * \param globalPos The global position
     */
       PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);
        priVars.setState(Indices::firstPhaseOnly);
        
        priVars[pressureIdx] = initialPressureField_(globalPos);
    
        //mole-fraction formulation
        priVars[Indices::switchIdx] = 1e-8;
        priVars[H2Idx] = 1e-8;
        

        //non-isothermal
        priVars[temperatureIdx] = initialTemperatureField_(globalPos);
        
        return priVars;
    }
    
    
    Scalar initialTemperatureField_(const GlobalPosition globalPos) const
    {
        return surfaceTemperature_ + (reservoirBottom_ - globalPos[1])*0.03;
    }
    
    
     Scalar initialPressureField_(const GlobalPosition globalPos) const
    {
        const Scalar densityW = 1000;
        return 1.0e5 - densityW*this->gravity()[dimWorld-1]*(reservoirBottom_ - globalPos[dimWorld-1]);
    }
    
    
   

    static constexpr Scalar eps_ = 1e-6;
    Scalar h2X_;
    Scalar time_;
    Scalar injectionDuration_;
    Scalar reservoirBottom_;
    Scalar surfacePressure_;
    Scalar surfaceTemperature_;
    Scalar injectionPressure_ ;
    Scalar co2Temperature_;    
    Scalar co2Pressure_;
    Scalar h2Temperature_;
    Scalar h2Pressure_;
    Scalar permeability_;
    Scalar co2Pressurepro_;
    Scalar h2Pressurepro_;
    
    
    std::string name_ ;
    std::vector<double> temperature_;



};

} //end namespace Dumux

#endif
