# Summary 

This is the DuMuX module containing the code for producing the results
published in:

A. Al Assadi
Numerical simulations of underground carbon dioxide storage with hydrogen impurities
Master's thesis, Institut für Wasser- und Umweltsystemmodellierung, Universität Stuttgart, 7/2018.


# Installation


DuMuX Version: dumux-course-2018 (tag)

How to install ?: install DuMuX like described  in the user handbook and git checkout  
Install DuMuX like described  in the user handbook and ----git checkout dumux-course-2018----.

Dune (dune-common,	dune-istl, dune-localfunctions,	dune-uggrid, dune-geometry,	dune-common)
Versions: 2.6, install like described in the DuMuX user handbook.

CMake Version: 3.5.1


git clone https://git.iws.uni-stuttgart.de/dumux-pub/AlAssadi2018a.git




# Results

### Henry’s law versus Duan and Sun model
For Henry's law: (Figure 4.1 & 4.3)

run Main_ccs_Henry 

For Duan and Sun model: (Figure 4.2 & 4.4) 

run Main_ccs_Duan


### Peaceman well model

For Peaceman well model: (Figure 4.5/4.6/4.7/4.8)

### Accumulation of H2 during the CCS

For Accumulation of H2 during CCS: (Figure 4.9/4.10/4.11/4.12/4.13/4.14)

run CCS_Operation

### CO2 Plume Geothermal 

For CO2 Plume Geothermal: (Figure 4.15/4.16/4.17/4.18/4.19/4.20//4.21)

run CPG_Operation


